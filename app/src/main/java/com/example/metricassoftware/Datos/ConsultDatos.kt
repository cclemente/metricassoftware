package com.example.metricassoftware.Datos
data class ConsultDatos(var uno:Int, var dos:String,var tres:Int,var cuatro:Int,var cinco:Int,var seis:Int,var siete:Int, var ocho:Int, var nueve:Double, var diez:Double, var once:Double, var doce:Double, var trece:Double, var catorce:Double) {
    var id:Int=uno
    var nomArch:String=dos
    var n_operadores:Int=tres
    var total_operadores:Int=cuatro
    var n_operandos:Int=cinco
    var total_operandos:Int=seis
    var long_programa:Int=siete
    var vocabulario:Int=ocho
    var volumen:Double=nueve
    var dificultad:Double=diez
    var nivel:Double=once
    var esfuerzo:Double=doce
    var tiempo:Double=trece
    var bugs:Double=catorce
}
