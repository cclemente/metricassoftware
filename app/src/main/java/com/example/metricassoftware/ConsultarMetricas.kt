package com.example.metricassoftware

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.metricassoftware.Adapters.ConsultaAdapter
import com.example.metricassoftware.Datos.ConsultDatos
import kotlinx.android.synthetic.main.consultametricas.*

class ConsultarMetricas: AppCompatActivity() {
    private var ResultadoConsulta: ArrayList<ConsultDatos> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.consultametricas)
        consulta()
        buttoninicio2.setOnClickListener {
            finish()
        }
    }
    fun consulta(){
        val admin = AdminSQLiteOpenHelper(this,"almacendemetricas",null,1)
        val bd = admin.writableDatabase
        val fila = bd.rawQuery("select * from codigos", null)
        if (fila.moveToFirst()) {
            do {
                var uno = ConsultDatos(fila.getString(0).toInt(),fila.getString(1),fila.getString(2).toInt(),fila.getString(3).toInt(),fila.getString(4).toInt(),fila.getString(5).toInt(),fila.getString(6).toInt(),fila.getString(7).toInt(),fila.getString(8).toDouble(),fila.getString(9).toDouble(),fila.getString(10).toDouble(),fila.getString(11).toDouble(),fila.getString(12).toDouble(),fila.getString(13).toDouble())
                ResultadoConsulta.add(uno)
                Log.i("id0",fila.getString(0))
                Log.i("id1",fila.getString(1))
                Log.i("id2",fila.getString(2))
                Log.i("id3",fila.getString(3))
                Log.i("id4",fila.getString(4))
                Log.i("id5",fila.getString(5))
                Log.i("id6",fila.getString(6))
                Log.i("id7",fila.getString(7))
                Log.i("id8",fila.getString(8))
                Log.i("id9",fila.getString(9))
                Log.i("id10",fila.getString(10))
                Log.i("id11",fila.getString(11))
                Log.i("id12",fila.getString(12))
                Log.i("id13",fila.getString(13))
                Log.i("id13",fila.count.toString())
                Log.i("id13",fila.columnCount.toString())
            }while (fila.moveToNext())
            val recyclerView = findViewById<RecyclerView>(R.id.ConsultaMetricasRecycler)
            recyclerView.setHasFixedSize(true)
            var myAdapter=ConsultaAdapter(ResultadoConsulta,this)
            recyclerView.layoutManager= LinearLayoutManager(this)
            recyclerView.adapter = myAdapter
        } else
            Toast.makeText(this, "No existe un artículo con dicho código",  Toast.LENGTH_SHORT).show()
        bd.close()
    }
}