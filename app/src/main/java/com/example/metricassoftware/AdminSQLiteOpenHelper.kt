package com.example.metricassoftware

import android.content.Context
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteDatabase.CursorFactory

class AdminSQLiteOpenHelper(context: Context, name: String, factory: CursorFactory?, version: Int) : SQLiteOpenHelper(context, name, factory, version) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("create table codigos(id integer primary key autoincrement not null, nomArch text, n_operadores int,total_operadores int, n_operandos int, total_operandos int, long_programa int, vocabualrio int,volumen real, dificultad real,nivel real, esfuerzo real, tiempo real, bugs real)")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }
}
