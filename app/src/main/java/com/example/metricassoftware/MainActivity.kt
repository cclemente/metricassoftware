package com.example.metricassoftware
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener {
            val intent2 = Intent(this, CalcularMetricas::class.java)
            startActivity(intent2)
        }
        ConsultaButton.setOnClickListener {
            val intent2 = Intent(this, ConsultarMetricas::class.java)
            startActivity(intent2)
        }
        creditos.setOnClickListener {
            val intent2 = Intent(this, CreditodActivity::class.java)
            startActivity(intent2)
        }
    }
}
