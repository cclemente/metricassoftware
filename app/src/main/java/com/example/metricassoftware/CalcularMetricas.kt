package com.example.metricassoftware

import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils.substring
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.metricassoftware.Adapters.ResultAdapter
import com.example.metricassoftware.Datos.ResulDatos
import kotlinx.android.synthetic.main.calcularmetricas.*
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.math.log2
import kotlin.math.pow


class CalcularMetricas: AppCompatActivity() {
    private val READ_REQUEST_CODE: Int = 42
    private var resultados: ArrayList<ResulDatos> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.calcularmetricas)
        codigo.visibility= View.INVISIBLE
        GuardarButton.visibility=View.INVISIBLE
        seleccionar.setOnClickListener {
            performFileSearch()
        }
        buttoninicio.setOnClickListener {
            finish()
        }
    }
    fun performFileSearch() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "application/*"
        }
       startActivityForResult(intent, READ_REQUEST_CODE)
    }

    @Throws(IOException::class)
    private fun readTextFromUri(uri: Uri): String {
        val stringBuilder = StringBuilder()
        contentResolver.openInputStream(uri)?.use { inputStream ->
            BufferedReader(InputStreamReader(inputStream)).use { reader ->
                var line: String? = reader.readLine()
                while (line != null) {
                    stringBuilder.append(line)
                    stringBuilder.append("\n")
                    line = reader.readLine()
                }
            }
        }
        return stringBuilder.toString()
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        super.onActivityResult(requestCode, resultCode, resultData)
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            resultData?.data?.also { uri ->
                var resultado:String=resultData.dataString.toString()
                resultado=resultado.substring(resultado.lastIndexOf("F")+1)
                val result = readTextFromUri(uri);
                seleccionar.visibility = View.INVISIBLE
                codigo.text = result
                codigo.visibility = View.VISIBLE

                //ESR
                var elementos = result
                var listOperadores: ArrayList<String> = ArrayList()
                var listOperandos: ArrayList<String> = ArrayList()

                //Quitar caracteres de control
                elementos = elementos.replace("\n"," ")
                elementos = elementos.replace("\t"," ")
                elementos = elementos.replace("\b"," ")
                elementos = elementos.replace("\r"," ")
                elementos = elementos.trim()

                val operadoresPy = arrayOf(
                    ">>=","<<=","//=","\\*\\*=",
                    "\\+=","-=","\\*=","/=","%=","<<",">>","==","!=","\\*\\*","<=",">=","//","&=","\\|=","\\^=",
                    "<",">","\\+","-","\\*","/","%", "&","\\|", "\\^","~","\\[","\\]","\\(","\\)",",",":","="
                )

                for(operador in operadoresPy){
                    val pattern: Pattern = Pattern.compile(operador)
                    val matcher: Matcher = pattern.matcher(elementos)

                    val nuevoOperador = operador.replace("\\","")
                    while (matcher.find()) {
                        listOperadores.add(nuevoOperador)
                    }
                    elementos = elementos.replace(nuevoOperador, " ")
                    println(elementos)
                }

                val regex = """ +""".toRegex()
                var listElementos =  regex.split(elementos)

                val operadoresPyCompleto = arrayOf(
                    "==","!=","<",">","<=",">=","+","-","*","**",
                    "/","//","%", "&","|","^","~","<<",">>", "[",
                    "]","(",")",",",":","=",
                    "and","del","for","is","raise",
                    "assert","if","else","elif","from",
                    "from","lambda","return","break",
                    "global","not","try","class","except",
                    "or","while","continue","exec","import",
                    "yield","def","finally","in","print"
                )

                for(elemento in listElementos){
                    if(operadoresPyCompleto.contains(elemento)){
                        listOperadores.add(elemento)
                    }else{
                        listOperandos.add(elemento)
                    }
                }

                //PARTE DE LOS OPERADORES

                resultados.add(ResulDatos("OPERADORES:", ""))

                var operandoresDist = listOperadores.distinct()

                var operadoresCierre = arrayOf(")","return","]")
                var N1 = 0
                var n1 = 0
                for (operadorD in operandoresDist){
                    if(!operadoresCierre.contains(operadorD)){
                        var num = 0
                        for(operador in listOperadores){
                            if(operadorD.equals(operador)){
                                num++
                            }
                        }
                        N1 += num
                        when(operadorD){
                            "[" -> resultados.add(ResulDatos("[ ]",num.toString()))
                            "(" -> resultados.add(ResulDatos("( )",num.toString()))
                            "def" -> resultados.add(ResulDatos("def ... return",num.toString()))
                            else -> {
                                resultados.add(ResulDatos(operadorD,num.toString()))
                            }
                        }

                        n1++
                    }
                }
                resultados.add(ResulDatos("Total(N1):",""+N1))
                resultados.add(ResulDatos("n1:",""+n1))

                resultados.add(ResulDatos("",""))

                resultados.add(ResulDatos("OPERANDOS:", ""))
                var operandosDist = listOperandos.distinct()

                var N2 = 0
                var n2 = 0
                for (operadosD in operandosDist){
                    var num = 0
                    for(operados in listOperandos){
                        if(operadosD.equals(operados)){
                            num++
                        }
                    }
                    N2 += num

                    resultados.add(ResulDatos(operadosD,num.toString()))
                    n2++
                }
                resultados.add(ResulDatos("Total(N2):",""+N2))
                resultados.add(ResulDatos("n2:",""+n2))

                resultados.add(ResulDatos("",""))

                //CALCULAR MÉTRICAS
                resultados.add(ResulDatos("METRICAS DERIVADAS",""))
                resultados.add(ResulDatos("---","---"))
                resultados.add(ResulDatos("NO. DE OPERADORES","$n1"))
                resultados.add(ResulDatos("TOTAL DE OPERADORES","$N1"))
                resultados.add(ResulDatos("NO. DE OPERANDOS","$n2"))
                resultados.add(ResulDatos("TOTAL DE OPERANDOS","$N2"))
                resultados.add(ResulDatos("---","---"))
                val N = N1 + N2
                resultados.add(ResulDatos("LONGITUD DEL PRO.", "$N (TOKENS)"))
                val n = n1 + n2
                resultados.add(ResulDatos("VOCABULARIO DE P","$n"))
                val V = N * log2(n.toDouble())
                resultados.add(ResulDatos("VOLUMEN","$V (BITS)"))
                val D = (n1/2.0)*(N2/n2.toDouble());
                resultados.add(ResulDatos("DIFICULTAD","$D"))
                val L = 1/D.toDouble();
                resultados.add(ResulDatos("NIVEL","$L"))
                val E = D*V;
                resultados.add(ResulDatos("ESFUERZO","$E"))
                val T = E/18;
                resultados.add(ResulDatos("TIEMPO","$T (SEGUNDOS)"))
                val B = (E.pow(2/3))/3000
                resultados.add(ResulDatos("BUGS","$B"))

                val recyclerView = findViewById<RecyclerView>(R.id.resultadosrecycler)
                recyclerView.setHasFixedSize(true)
                var myAdapter=ResultAdapter(resultados)
                recyclerView.layoutManager=LinearLayoutManager(this)
                recyclerView.adapter = myAdapter
                GuardarButton.visibility=View.VISIBLE

                GuardarButton.setOnClickListener {
                    almacenarMetricas(resultado,n1,N1,n2,N2,N,n,V,D.toDouble(),L.toDouble().toDouble(),E,T,B)
                }
            }
        }
    }
    fun almacenarMetricas(
        nombre:String, n_operandores: Int,
        t_operadores:Int,
        n_operandos:Int,
        t_operandos: Int,
        long_programa: Int,
        vocabulario: Int,
        volumen:Double, dificultad:Double, nivel: Double, esfuerzo:Double, tiempo:Double,
        bugs:Double ){
        var metrica = AdminSQLiteOpenHelper(this,"almacendemetricas",null,1)
        var bd=metrica.writableDatabase
        var registro=ContentValues()
      //  registro.put("id",0)
        registro.put("nomArch",nombre)
        registro.put("n_operadores",n_operandores)
        registro.put("total_operadores",t_operadores)
        registro.put("n_operandos",n_operandos)
        registro.put("total_operandos",t_operandos)
        registro.put("long_programa",long_programa)
        registro.put("vocabualrio",vocabulario)
        registro.put("volumen",volumen)
        registro.put("dificultad",dificultad)
        registro.put("nivel",nivel)
        registro.put("esfuerzo",esfuerzo)
        registro.put("tiempo",tiempo)
        registro.put("bugs",bugs)
        bd.insert("codigos",null,registro)
        bd.close()
        Toast.makeText(this, "Se guardaron las metricas correctamente",  Toast.LENGTH_SHORT).show()
        conulta()


    }
    fun conulta(){
        val admin = AdminSQLiteOpenHelper(this,"almacendemetricas",null,1)
        val bd = admin.writableDatabase
        val fila = bd.rawQuery("select * from codigos", null)
        fila.moveToFirst()
        do {
            Log.i("miconsulta",fila.getString(1))
        }while (fila.moveToNext())
        if (fila.moveToFirst()) {
        } else
            Toast.makeText(this, "No existe un artículo con dicho código",  Toast.LENGTH_SHORT).show()
        bd.close()
    }
}