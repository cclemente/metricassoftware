package com.example.metricassoftware.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.metricassoftware.Datos.ResulDatos
import com.example.metricassoftware.R

class ResultAdapter(private val items: ArrayList<ResulDatos>) :
    RecyclerView.Adapter<ResultAdapter.MyViewHolder>() {
    override fun getItemCount(): Int {
        return items.size
    }
    override fun onBindViewHolder(itemsViewHolder: MyViewHolder, i: Int) {
        itemsViewHolder.vTitle.text = items[i].titulo
        itemsViewHolder.vResultado.text=items[i].resultado
    }
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val itemView = LayoutInflater.from(viewGroup.context).inflate(R.layout.linearesult, viewGroup, false)
        return MyViewHolder(itemView)
    }

    class MyViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var vTitle: TextView = v.findViewById(R.id.Titulotext)
        var vResultado: TextView = v.findViewById(R.id.Resultadotext)
    }
}
