package com.example.metricassoftware.Adapters

import android.content.Context
import android.os.Build
import android.transition.Slide
import android.transition.TransitionManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.metricassoftware.AdminSQLiteOpenHelper
import com.example.metricassoftware.Datos.ConsultDatos
import com.example.metricassoftware.R


class ConsultaAdapter(private val items: ArrayList<ConsultDatos>,context: Context) :
    RecyclerView.Adapter<ConsultaAdapter.MyViewHolderr>() {
    var bandera=0
    var mcontext=context
    lateinit var viewGroup:ViewGroup
    private lateinit var popupWindow:PopupWindow

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(itemsViewHolder: MyViewHolderr, i: Int) {
            itemsViewHolder.EliminarButton.setOnClickListener {
                val admin = AdminSQLiteOpenHelper(mcontext,"almacendemetricas",null,1)
                val bd = admin.writableDatabase
                val cant = bd.delete("codigos", "id=${items[i].id.toString()}", null)
                bd.close()
                if (cant == 1)
                    Toast.makeText(mcontext, "Se borró el artículo con dicho código", Toast.LENGTH_SHORT).show()
                else
                    Toast.makeText(mcontext, "No existe un artículo con dicho código", Toast.LENGTH_SHORT).show()
                items.remove(items[i])
                notifyItemRemoved(i)
                notifyDataSetChanged()
                //itemsViewHolder.linea.visibility=View.INVISIBLE
            }
        itemsViewHolder.vTitle.text = items[i].nomArch

        itemsViewHolder.linea.setOnClickListener {
                if(bandera==0){
                    this.bandera=1
                    val inflater:LayoutInflater = mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                    // Inflate a custom view using layout inflater
                    val view = inflater.inflate(R.layout.resultadomensaje,null)
                    // Initialize a new instance of popup window
                    popupWindow = PopupWindow(
                        view, // Custom view to show in popup window
                        LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
                        LinearLayout.LayoutParams.WRAP_CONTENT // Window height
                    )
                    // Set an elevation for the popup window
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        popupWindow.elevation = 10.0F
                    }
                    // If API level 23 or higher then execute the code
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        // Create a new slide animation for popup window enter transition
                        val slideIn = Slide()
                        slideIn.slideEdge = Gravity.TOP
                        popupWindow.enterTransition = slideIn

                        // Slide animation for popup window exit transition
                        val slideOut = Slide()
                        slideOut.slideEdge = Gravity.RIGHT
                        popupWindow.exitTransition = slideOut
                    }

                    // Get the widgets reference from custom view
                    // Set a click listener for popup's button widget
                    view.findViewById<Button>(R.id.buttoncerrarmensaje).setOnClickListener {
                        popupWindow.dismiss()
                        this.bandera=0
                    }
                    view.findViewById<TextView>(R.id.tituloresultado).text=items[i].nomArch+ items[i].id.toString()
                    view.findViewById<TextView>(R.id.miresultado1).text=items[i].n_operadores.toString()
                    view.findViewById<TextView>(R.id.miresultado2).text=items[i].total_operadores.toString()
                    view.findViewById<TextView>(R.id.miresultado3).text=items[i].n_operandos.toString()
                    view.findViewById<TextView>(R.id.miresultado4).text=items[i].total_operandos.toString()
                    view.findViewById<TextView>(R.id.miresultado5).text=items[i].long_programa.toString()
                    view.findViewById<TextView>(R.id.miresultado6).text=items[i].vocabulario.toString()
                    view.findViewById<TextView>(R.id.miresultado7).text=items[i].volumen.toString()
                    view.findViewById<TextView>(R.id.miresultado8).text=items[i].dificultad.toString()
                    view.findViewById<TextView>(R.id.miresultado9).text=items[i].nivel.toString()
                    view.findViewById<TextView>(R.id.miresultado10).text=items[i].esfuerzo.toString()
                    view.findViewById<TextView>(R.id.miresultado11).text=items[i].tiempo.toString()
                    view.findViewById<TextView>(R.id.miresultado12).text=items[i].bugs.toString()
                    // Set a dismiss listener for popup window
                    popupWindow.setOnDismissListener {
                        //Toast.makeText(,"Sin conexión a internet 2", Toast.LENGTH_LONG).show()
                    }

                    // Finally, show the popup window on app
                    TransitionManager.beginDelayedTransition(viewGroup)
                    //TransitionManager.beginDelayedTransition(this@ConsultaAdapter)
                    //TransitionManager.beginDelayedTransition(recyclerView)
                    popupWindow.showAtLocation(
                        viewGroup, // Location to display popup window
                        Gravity.CENTER, // Exact position of layout to display popup
                        0, // X offset
                        0 // Y offset
                    )
            }

        }
    }
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolderr {
        this.viewGroup=viewGroup
        val itemView = LayoutInflater.from(viewGroup.context).inflate(R.layout.buttonconsulta, viewGroup, false)
        return MyViewHolderr(itemView)
    }

    class MyViewHolderr(v: View) : RecyclerView.ViewHolder(v) {
        var vTitle: TextView = v.findViewById(R.id.TituloCosulta)
        var linea:ConstraintLayout=v.findViewById(R.id.lineaResultado)
        var EliminarButton:Button=v.findViewById(R.id.ButtonDeleteConsulta)
        //var vResultado: TextView = v.findViewById(R.id.Resultadotext)
    }
}
